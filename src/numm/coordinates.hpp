// license here...

#ifndef NUMM_COORDINATES_HPP
#define NUMM_COORDINATES_HPP

#include    "config.hpp"

namespace numm
{

template<class T> 
struct coordinates {
    coordinates() {}
    coordinates( T xx, T yy ) : x( xx ), y( yy ) {}
    
    T x;
    T y;
};

} // numm

#endif
