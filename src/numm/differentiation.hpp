/* copyright Hans Hoogenboom and Esteban Tovagliari */

#include <iostream>
#include <vector>
#include <cmath>

#include "config.hpp"
#include "coordinates.hpp"


//create namespace NumM
namespace numm{


/****************************************************
*                                                   *
* Differentiation Algorithms, O(h) and O(h^2)       *
*                                                   *
****************************************************/

//forward divided difference, first till fourth derivative, O(h)
template< class T >
inline T fdd1h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+1)->y - i->y) / wdth;
    }
}


template< class T >
inline T fdd2h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+2)->y - 2 * (i+1)->y + i->y) / std::pow(wdth, 2);
    }
}


template< class T >
inline T fdd3h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+3)->y - 3 * (i+2)->y + 3 * (i+1)->y - i->y) / std::pow(wdth, 3);
    }
}


template< class T >
inline T fdd4h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+4)->y - 4 * (i+3)->y + 6 * (i+2)->y - 4 * (i+1)->y + i->y) / std::pow(wdth, 4);
    }
}


//backward divided difference, first till fourth derivative, O(h)
template< class T >
inline T bdd1h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (i->y - (i-1)->y) / wdth;
    }
}


template< class T >
inline T bdd2h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (i->y - 2 * (i-1)->y + (i-2)->y) / std::pow(wdth, 2);
    }
}


template< class T >
inline T bdd3h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (i->y - 3 * (i-1)->y + 3 * (i-2)->y - (i-3)->y) / std::pow(wdth, 3);
    }
}


template< class T >
inline T bdd4h1( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (i->y - 4 * (i-1)->y + 6 * (i-2)->y - 4 * (i-3)->y + (i-4)->y) / std::pow(wdth, 4);
    }
}


//central divided difference, first till fourth derivative, O(h^2)
template< class T >
inline T cdd1h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+1)->y - (i-1)->y) / (2 * wdth);
    }
}


template< class T >
inline T cdd2h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+1)->y - 2 * i->y + (i-1)->y) / std::pow(wdth, 2);
    }
}


template< class T >
inline T cdd3h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+2)->y - 2 *(i+1)->y + 2 * (i-1)->y - (i-2)->y) / (2 * std::pow(wdth, 3));
    }
}


template< class T >
inline T cdd4h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = ((i+2)->y - 4 *(i+1)->y + 6 * i->y - 4 * (i-1)->y + (i-2)->y) / std::pow(wdth, 4);
    }
}


/****************************************************
*                                                   *
* Differentiation Algorithms, O(h^2) and O(h^4)     *
*                                                   *
****************************************************/


//forward divided difference, first till fourth derivative, O(h^2)
template< class T >
inline T fdd1h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-(i+2)->y + 4 * (i+1)->y - 3 * i->y) / (2 * wdth);
    }
}


template< class T >
inline T fdd2h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-(i+3)->y + 4 * (i+2)->y - 5 * (i+1)->y + 2 * i->y) / std::pow(wdth, 2);
    }
}


template< class T >
inline T fdd3h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-3 * (i+4)->y + 14 * (i+3)->y - 24 * (i+2)->y + 18 * (i+1)->y - 5 * i->y) / (2 * std::pow(wdth, 3));
    }
}


template< class T >
inline T fdd4h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-2 * (i+5)->y + 11 * (i+4)->y - 24 * (i+3)->y + 26 * (i+2)->y - 14 * (i+1)->y + 3 * i->y) / std::pow(wdth, 4);
    }
}


//backward divided difference, first till fourth derivative, O(h^2)
template< class T >
inline T bdd1h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (3 * i->y - 4 * (i-1)->y + (i-2)->y) / (2 * wdth);
    }
}


template< class T >
inline T bdd2h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (2 * i->y - 5 * (i-1)->y + 4 * (i-2)->y - (i-3)->y) / std::pow(wdth, 2);
    }
}


template< class T >
inline T bdd3h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (5 * i->y - 18 * (i-1)->y + 24 * (i-2)->y - 14 * (i-3)->y + 3 * (i-4)->y) / ( 2 * std::pow(wdth, 3));
    }
}


template< class T >
inline T bdd4h2( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (3 * i->y - 14 * (i-1)->y + 26 * (i-2)->y - 24 * (i-3)->y + 11 * (i-4)->y) / std::pow(wdth, 4);
    }
}


//central divided difference, first till fourth derivative, O(h^4)
template< class T >
inline T cdd1h4( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-(i+2)->y + 8 * (i+1)->y - 8 * (i-1)->y + (i-2)->y) / (12 * wdth);
    }
}


template< class T >
inline T cdd2h4( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-(i+2)->y + 16 * (i+1)->y - 30 * i->y + 16 * (i-1)->y -(i-2)->y) / (12 * std::pow(wdth, 2));
    }
}


template< class T >
inline T cdd3h4( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-(i+3)->y + 8 * (i+2)->y - 13 *(i+1)->y + 13 * (i-1)->y - 8 * (i-2)->y + (i+3)->y) / (8 * std::pow(wdth, 3));
    }
}


template< class T >
inline T cdd4h4( typename std::vector< numm::coordinates< T > >::iterator i, typename std::vector< numm::coordinates< T > >::iterator v_end, int counter, T wdth, typename std::vector< numm::coordinates< T > > &sol ){
    for( ; i < v_end; ++i, ++counter ){
        sol[counter].x = i->x;
        sol[counter].y = (-(i+3)->y + 12 * (i+2)->y - 39 * (i+1)->y + 56 * i->y - 39 * (i-1)->y + 12 * (i-2)->y - (i-3)->y) / (6 * std::pow(wdth, 4));
    }
}


/****************************************************
*                                                   *
* Algorithm interface                               *
*                                                   *
****************************************************/



//differentiation algorithms, backward, central and forward difference
template< class T >
void diffOh1 ( int deriv, T x1, T x2, int steps, T (*func)( T ), std::vector< numm::coordinates< T > > &sol ){
    sol.clear();
    sol.resize( steps + 1 );
    T wdth = (x2 - x1) / steps;

    //generate function values
    std::vector< numm::coordinates< T > > fval;
    fval.resize( steps + 1 );
    int counter = 0;
    typename std::vector< numm::coordinates< T > >::iterator v_begin = fval.begin();
    typename std::vector< numm::coordinates< T > >::iterator v_end   = fval.end();
    for( typename std::vector< numm::coordinates< T > >::iterator i = v_begin; i < v_end; ++i, ++counter ){
        i->x = x1 + counter * wdth;
        i->y = func( i->x );
    }

    switch( deriv ){
        case 1:
            //3 inline function calls using iterators
            //first point, forward divided difference`
            counter = 0;
            numm::fdd1h1( v_begin, v_begin+1, counter, wdth, sol );
            //intermediate points
            counter = 1;
            numm::cdd1h2( v_begin+1, v_end-1, counter, wdth, sol );
            //last point, backward divided difference
            counter = steps;
            numm::bdd1h1( v_end-1, v_end, counter, wdth, sol );
            break;
        case 2:
            counter = 0;
            numm::fdd2h1( v_begin, v_begin+1, counter, wdth, sol );
            counter = 1;
            numm::cdd2h2( v_begin+1, v_end-1, counter, wdth, sol );
            counter = steps;
            numm::bdd2h1( v_end-1, v_end, counter, wdth, sol );
            break;
        case 3:
            counter = 0;
            numm::fdd3h1( v_begin, v_begin+2, counter, wdth, sol );
            counter = 2;
            numm::cdd3h2( v_begin+2, v_end-2, counter, wdth, sol );
            counter = steps - 1;
            numm::bdd3h1( v_end-2, v_end, counter, wdth, sol );
            break;
        case 4:
            counter = 0;
            numm::fdd4h1( v_begin, v_begin+2, counter, wdth, sol );
            counter = 2;
            numm::cdd4h2( v_begin+2, v_end-2, counter, wdth, sol );
            counter = steps - 1;
            numm::bdd4h1( v_end-2, v_end, counter, wdth, sol );
            break;
    } 
}



template< class T >
void diffOh2 ( int deriv, T x1, T x2, int steps, T (*func)( T ), std::vector< numm::coordinates< T > > &sol ){
    sol.clear();
    sol.resize( steps + 1 );
    T wdth = (x2 - x1) / steps;

    //generate function values
    std::vector< numm::coordinates< T > > fval;
    fval.resize( steps + 1 );
    int counter = 0;
    typename std::vector< numm::coordinates< T > >::iterator v_begin = fval.begin();
    typename std::vector< numm::coordinates< T > >::iterator v_end   = fval.end();
    for( typename std::vector< numm::coordinates< T > >::iterator i = v_begin; i < v_end; ++i, ++counter ){
        i->x = x1 + counter * wdth;
        i->y = func( i->x );
    }

    switch( deriv ){
        case 1:
            counter = 0;
            numm::fdd1h2( v_begin, v_begin+2, counter, wdth, sol );
            counter = 2;
            numm::cdd1h4( v_begin+2, v_end-2, counter, wdth, sol );
            counter = steps-1;
            numm::bdd1h2( v_end-2, v_end, counter, wdth, sol );
            break;
        case 2:
            counter = 0;
            numm::fdd2h2( v_begin, v_begin+2, counter, wdth, sol );
            counter = 2;
            numm::cdd2h4( v_begin+1, v_end-2, counter, wdth, sol );
            counter = steps-1;
            numm::bdd2h2( v_end-2, v_end, counter, wdth, sol );
            break;
        case 3:
            counter = 0;
            numm::fdd3h2( v_begin, v_begin+3, counter, wdth, sol );
            counter = 3;
            numm::cdd3h4( v_begin+3, v_end-3, counter, wdth, sol );
            counter = steps - 2;
            numm::bdd3h2( v_end-3, v_end, counter, wdth, sol );
            break;
        case 4:
            counter = 0;
            numm::fdd4h2( v_begin, v_begin+3, counter, wdth, sol );
            counter = 3;
            numm::cdd4h4( v_begin+3, v_end-3, counter, wdth, sol );
            counter = steps - 2;
            numm::bdd4h2( v_end-3, v_end, counter, wdth, sol );
            break;
    } 
}


} //end namespace numm
