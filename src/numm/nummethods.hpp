#include <iosttream>
#include <vector>
#include <cmath>

//create namespace NumM
namespace NumM{


/****************************************************
*                                                   *
* General constants, functions or data              *
*                                                   *
****************************************************/


const int MaxIter  = 30;
const float MaxErr = 0.001;


/****************************************************
*                                                   *
* Interpolation Algorithms                          *
*                                                   *
****************************************************/


template<class T> 
struct coordinates {
    T x;
    T y;
};


//newton's divided difference method
template<class T>
T newDivPol( T x, std::vector< coordinates<T> > pos ){//pass by value
    //std::vector< coordinates<T> > pos = orig_pos;
    int pos_sz = pos.size();
    //vector to hold solutions
    std::vector<T> sol(pos_sz, pos[0].y);
    int col_sz = pos_sz;
    int row_sz = pos_sz;
    //calculate differences and store in sol
    //itertate over virtual columns
    for( int i = 1; i < col_sz; i++ ){
        row_sz--;
        //iterate over rows
        for( int j = 0; j < row_sz; j++ ){
            pos[j].y = (pos[j+1].y - pos[j].y) / (pos[i+j].x - pos[j].x);
            if( j == 0 ){
                sol[i] = pos[j].y;
            }
        }
    }
    //calculate f(x)
    T fx = sol[0];
    T ff = T(1);
    for( int i = 1; i < pos_sz; i++ ){
        ff *= (x - pos[i-1].x);
        fx += sol[i] * ff;
    }
    return fx;
}
        

//lagrange interpolating polynomials
//add option to choose number of interpolation points
template<class T>
T lagrangePol( T x, const std::vector< coordinates<T> > &pos ){
    T result = T(0);
    for( typename std::vector< coordinates<T> >::const_iterator i = pos.begin(), e = pos.end(); i != e; ++i ){
        T lagInt = T(1);
        for( typename std::vector< coordinates<T> >::const_iterator j = pos.begin(); j != e; ++j ){
            if( j != i ){
                lagInt *= (x - j->x) / (i->x - j->x);
            }
        }
        result += lagInt * i->y;
    }
    return result;
}


//spline interpolations
//use a system of linear equations -> port from python to c++
//but not like crude functions like those above and the others
//from the num_method book...templated functions? (friend) methods?
//linear
template<class T>
T linSplinePol( T x, const std::vector< coordinates<T> > &pos ){
    typename std::vector< coordinates<T> >::const_iterator idx = pos.begin();
    typename std::vector< coordinates<T> >::const_iterator pos_end = pos.end();
    //find the position for which x0 < x < x1
    while( (idx < pos_end) && (idx->x < x) ){
        idx++;
    }
    T mid = (idx->y - (idx-1)->y) / (idx->x -(idx-1)->x);
    return (idx-1)->y + mid * (x - (idx-1)->x);
}
//quadratic
//cubic


/****************************************************
*                                                   *
* Root finding Algorithms                           *
*                                                   *
****************************************************/


//two methods for solving the root with an a-priori knowledge
//of the interval that contains the root of the equation
float bisection(float x0, float x1, float (*func)( const float ) ){
    float fx1, fxm, fps;
    float xn = 0.5 * (x0 + x1);
    float xm = 0.0f;
    int iter = 0;
    while( (std::abs( (xn - xm) * 100.0 / xn ) > MaxErr) && ( iter < MaxIter ) ){
        fx1 = func( x1 );
        xm  = xn;
        fxm = func( xm );
        fps = fxm * fx1;
        if( fps > 0 ){
            x1 = xm;
        } else {
            x0 = xm;
        }
        iter++;
        xn = 0.5 * (x0 + x1);
    }
    disp_result(iter, xm, func);
    return xm;
}


float falsepos( float x0, float x1, float (*func)( const float ) ){
    float fx0, fx1, fxm, fps;
    float xn = 2.0 * x0 + 1.0;
    float xm = x0;
    int iter = 0;
    while( (std::abs( (xn - xm) * 100 / xn ) > MaxErr) && ( iter < MaxIter ) ){
        fx0 = func( x0 );
        fx1 = func( x1 );
        xn  = xm;
        xm  = (x0 * fx1 - x1 * fx0) / (fx1 - fx0);
        fxm = func( xm );
        fps = fxm * fx1;
        if( fxm > 0 ){
            x1 = xm;
        } else {
            x0 = xm;
        }
        iter++;
    }   
    disp_result(iter, xm, func);
    return xm;
}


//iterative or open methods to solve the root of an equation
//one point only usefull if you know the equation
float one_point_iter( float x0, float (*func)( float ) ){
    float x1 = 0.1f;
    int iter = 0;
    while( ( std::abs((x1 - x0) * 100 / x1) > MaxErr ) && ( iter < MaxIter ) ){
        x0 = x1;
        x1 = 2.0f - std::exp(x0 / 4.0f);
        iter++;
    }
    disp_result(iter, x1, func);
    return x1;
}

//based on use of taylor series, first order approximation
float newton_raphson( float x0, float (*func)( float ) ){
    //implement central difference method for differentiation
    //atm we have a hard coded derivative (dfx)
    float fx, dfx;
    float dx = x0;
    int iter = 0;
    while( ( std::abs(dx * 100.0f / x0) > MaxErr ) && ( iter < MaxIter ) ){
        fx = func( x0 );
        dfx = -1.0f * std::exp(-x0 / 4.0f) * (1.5f - x0 / 4.0f);
        dx  = -fx / dfx;
        x0 += dx;
        iter++;
    }
    disp_result( iter, x0, func );
    return x0;
}


float secant( float x0, float x1, float (*func)( float )){
    //crude algorithm to determine first derivative
    float f0, f1;
    float dx = x0;
    int iter = 0;
    while( ( std::abs(dx * 100.0f / x0) > MaxErr ) && ( iter < MaxIter ) ){
        f0 = func( x0 );
        f1 = func( x1 );
        dx = -f1 / ((f0 -f1) / (x0 - x1));
        x0 = x1;
        x1 += dx;
        iter++;
    }
    disp_result( iter, x0, func );
    return x0;
}


//TODO
//iterative methods for a system of non-linear equations
void nl_dir_iter(){}


/****************************************************
*                                                   *
* Integration Algorithms                            *
*                                                   *
****************************************************/


//Integration algorithms, non iterative
//composite Trapezoidal integration
float compTrapInt( const float x0, const float x1, const int steps, float (*func)( float ), const int dbg = 0 ){
    int step;
    (steps == 0) ? step = 1 : step = steps;
    float h = (x1 - x0) / step;
    float Isum = 0.0;
    for( int i = 1; i < step; i++ ){
        Isum += func( x0 + h * i );
    }
    return 0.5 * h * (func( x0 ) + func( x1 ) + 2 * Isum);
}


//composite Simpson's Rule Integration
//steps has to be an even number
float compSimpsInt( const float x0, const float x1, const int steps, float (*func)( float ), const int dbg = 0 ){
    int even_step;
    if( steps == 0 ) {
        even_step = 2;
    } else {
        even_step = (int)std::abs( steps ) % 2;
        even_step += std::abs( steps );
    }
    float h = (x1 - x0) / even_step;
    float Isum1 = 0.;
    float Isum2 = 0.;
    for( int i = 1; i < even_step; i += 2 ){
        Isum1 += func( x0 + h * i );
    }
    for(int i = 2; i < even_step - 1; i += 2 ){
        Isum2 += func( x0 + h * i );
    }
    return (h / 3.0) * ( func( x0 ) + func( x1 ) + 4 * Isum1 + 2 * Isum2  );
}


struct gaussPoints{
    int pntnum;
    std::vector<float> natc;
    std::vector<float> wght;

    gaussPoints( const int pntnum ){
            //error checking stuff to see if
            //pntnum > 0 and < 7
            natc.resize(pntnum);
            wght.resize(pntnum);
            switch( pntnum ){
            case 1:
                natc[0] = 0.0;

                wght[0] = 2.0;
            case 2:
                natc[0] = -0.5773502692;
                natc[1] =  0.5773502692;

                wght[0] =  1.0;
                wght[1] =  1.0;
            case 3:
                natc[0] = -0.7745966692;
                natc[1] =  0.0000000000;
                natc[2] =  0.7745966692;

                wght[0] =  0.5555555556;
                wght[1] =  0.8888888889;
                wght[2] =  0.5555555556;
            case 4:
                natc[0] = -0.8611363116;
                natc[1] = -0.3399810436;
                natc[2] =  0.3399810436;
                natc[3] =  0.8611363116;

                wght[0] =  0.3478548451;
                wght[1] =  0.6521451549;
                wght[2] =  0.6521451549;
                wght[3] =  0.3478548451;
            case 5:
                natc[0] = -0.9061795459;
                natc[1] = -0.5384693101;
                natc[2] =  0.0000000000;
                natc[3] =  0.5384693101;
                natc[4] =  0.9061795459;

                wght[0] =  0.2369268850;
                wght[1] =  0.4786286705;
                wght[2] =  0.5688888889;
                wght[3] =  0.4786286705;
                wght[4] =  0.2369268850;
            case 6:
                natc[0] = -0.9324695142;
                natc[1] = -0.6612093865;
                natc[2] = -0.2386191861;
                natc[3] =  0.2386191861;
                natc[4] =  0.6612093865;
                natc[5] =  0.9324695142;

                wght[0] =  0.1743244924;
                wght[1] =  0.3607615730;
                wght[2] =  0.4679139346;
                wght[3] =  0.4679139346;
                wght[4] =  0.3607615730;
                wght[5] =  0.1743244924;
            }
    }   
};


float gaussInt( const float x0, const float x1, const int ngp, float (*func)( float ), const int dbg = 0 ){
    float e_mid = 0.5 * (x0 + x1);
    float e_wdt = 0.5 * (x1 - x0);
    float x;
    float I = 0.;
    gaussPoints gpv(ngp);
    for( int i = 0; i < ngp; i++ ){
        x = e_mid + e_wdt * gpv.natc[i];
        I += gpv.wght[i] * func( x );
    }
    return I * e_wdt;
}


/*
//TODO: Romberg Integration
//eps is the fault tolerance
float rombergInt( const float x0, const float x1, const float eps, float (*func)( float ), const int dbg = 0 ){
    int n = 0;
    //float solLow = 0;
    //float solHgh = 0;
    //float Q    = 1;
    int i, steps = 0;
    int j = 1;

    //basic solution matrix (triangluar matrix)
    typedef std::vector<float> result ;
    //initialize with two result vectors
    std::vector<result> res_mat;
    res_mat.assign(2, std::vector<float> );
    res_mat[0].push_back(2);
    res_mat[1].push_back(1);
    res_mat[0][0] = compTrapInt( x0, x1, steps, func );
    res_mat[1][0] = compTrapInt( x0, x1, steps, func );
    res_mat[0][1] = (std::pow(4, n) * res_mat[1][0] - res_mat[0][0]) / (std::pow(4, n) -1);
    while( ( std::abs((res_mat[i][j] - res_mat[i+1][j-1]) / res_mat[i][j]) * 100. > eps ) && ( n < MaxIter ) ){
            res_mat.push_back(1);
            res_mat[res_mat.size()-1].push_back(1);
            for( int k = 0; k < res_mat.size(); k++ ){
                res_mat[k].push_back(1);
            }
            res_mat[0][0] = compTrapInt( x0, x1, steps, func );
            res_mat[1][0] = compTrapInt( x0, x1, steps, func );
            //res_mat[0][1] = (std::pow(4, n) * solHgh - solLow) / (std::pow(4, n) -1);

    }
    float res = res_mat.front()[res_mat.front().begin()];
    return res;
}
*/


/****************************************************
*                                                   *
* Differentiation Algorithms                        *
*                                                   *
****************************************************/


/*TODO
//differentiation algorithms, backward, central and forward difference
//hard coded first till fourth derivative with error or order O(h) and O(h^2)
float dividedDiff( const float n, const float x0, const float x1, float (*func)( float ),
                   const int deriv_type = 1, const int deriv_order = 0, 
                   const bool order = 1, const int dbg = 0 ){
    float wdth = (x1 - x0) / (n - 1);
    float x    = x0;
    std::vector<float> points;
    for( int i = 0; i < n; i++ ){
        points.push_back( func( x ) );
        x += wdth;
    }
    //deriv_type 0,1,2 -> backward, central, forward
    //deriv_order 1st, 2nd, 3rd or 4th derivative
    switch( deriv_type ){
        case 0:
            switch( deriv_order ){
                case 0:
                    return (points[i+1] - points[i-1]) / (2 * wdth);
            }
    }
}
*/


/****************************************************
*                                                   *
* Ordinary Differential Equation solvers            *
*                                                   *
****************************************************/


void eulerODE( float x, float y, float wdth, int steps, float (*func)( float, float )){
    for( int i = 0; i < steps; i++ ){
        float slope = func( x, y );
        y += slope * wdth;
        x += wdth;
        std::cout << "X: " << x << "\t" << "Y: " << y << std::endl;

    }
}


void heunODE( float x, float y, float wdth, int steps, float (*func)( float, float )){
    for( int i = 0; i < steps; ++i ){
        float slope0 = func( x, y );
        float yt = y + slope0 * wdth;
        x = x + wdth;
        float slope1 = func( x, yt );
        float slope  = (slope0 + slope1) / wdth;
        y = y + slope * wdth;
        std::cout << "X: " << x << "\t" << "Y: " << y << std::endl;

    }
    std::cout << "X: " << x << "\t" << "Y: " << y << std::endl;
}


void rungaKutta2( float x, float y, float wdth, int steps, float (*func)( float, float ) ){
    for( int i = 0; i < steps; i++ ){
        float k1 = func( x, y );
        float xi = x + 0.5 * wdth;
        float yi = y + 0.5 * wdth * k1;
        float k2 = func( xi, yi );
              x += wdth;;
              y += wdth * k2;
        std::cout << "X: " << x << "\tY: " << y << std::endl;
    }
}


void rungaKutta3( float x, float y, float wdth, int steps, float (*func)( float, float ) ){
    for( int i = 0; i < steps; i++ ){
        float k1 = func( x, y );
        float xi = x + 0.5 * wdth;
        float yi = y + 0.5 * wdth * k1;
        float k2 = func( xi, yi );
              xi = x + wdth;
              yi = y - wdth * k1 + 2 * wdth * k2;
        float k3 = func( xi, yi );
              x += wdth;;
              y += (wdth / 6) * (k1 + 4 * k2 + k3);
        std::cout << "X: " << x << "\tY: " << y << std::endl;
    }
}


void rungaKutta4( float x, float y, float wdth, int steps, float (*func)( float, float ) ){
    for( int i = 0; i < steps; i++ ){
        float k1 = func( x, y );
        float xi = x + 0.5 * wdth;
        float yi = y + 0.5 * wdth * k1;
        float k2 = func( xi, yi );
              yi = y + 0.5 * wdth * k2;
        float k3 = func( xi, yi );
              xi = x + wdth;;
              yi = y + wdth * k3;
        float k4 = func( xi, yi );
        y += (wdth / 6 ) * (k1 + 2 * k2 + 2 * k3 + k4);
        x += wdth;
        std::cout << "X: " << x << "\tY: " << y << std::endl;
    }
}

} //end namespace NumM
