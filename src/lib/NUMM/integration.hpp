#include <iosttream>
#include <vector>
#include <cmath>

//create namespace NumM
namespace NumM{


/****************************************************
*                                                   *
* General constants, functions or data              *
*                                                   *
****************************************************/


const int MaxIter  = 30;
const float MaxErr = 0.001;


/****************************************************
*                                                   *
* Integration Algorithms                            *
*                                                   *
****************************************************/


//Integration algorithms, non iterative
//composite Trapezoidal integration
float compTrapInt( const float x0, const float x1, const int steps, float (*func)( float ), const int dbg = 0 ){
    int step;
    (steps == 0) ? step = 1 : step = steps;
    float h = (x1 - x0) / step;
    float Isum = 0.0;
    for( int i = 1; i < step; i++ ){
        Isum += func( x0 + h * i );
    }
    return 0.5 * h * (func( x0 ) + func( x1 ) + 2 * Isum);
}


//composite Simpson's Rule Integration
//steps has to be an even number
float compSimpsInt( const float x0, const float x1, const int steps, float (*func)( float ), const int dbg = 0 ){
    int even_step;
    if( steps == 0 ) {
        even_step = 2;
    } else {
        even_step = (int)std::abs( steps ) % 2;
        even_step += std::abs( steps );
    }
    float h = (x1 - x0) / even_step;
    float Isum1 = 0.;
    float Isum2 = 0.;
    for( int i = 1; i < even_step; i += 2 ){
        Isum1 += func( x0 + h * i );
    }
    for(int i = 2; i < even_step - 1; i += 2 ){
        Isum2 += func( x0 + h * i );
    }
    return (h / 3.0) * ( func( x0 ) + func( x1 ) + 4 * Isum1 + 2 * Isum2  );
}


struct gaussPoints{
    int pntnum;
    std::vector<float> natc;
    std::vector<float> wght;

    gaussPoints( const int pntnum ){
            //error checking stuff to see if
            //pntnum > 0 and < 7
            natc.resize(pntnum);
            wght.resize(pntnum);
            switch( pntnum ){
            case 1:
                natc[0] = 0.0;

                wght[0] = 2.0;
            case 2:
                natc[0] = -0.5773502692;
                natc[1] =  0.5773502692;

                wght[0] =  1.0;
                wght[1] =  1.0;
            case 3:
                natc[0] = -0.7745966692;
                natc[1] =  0.0000000000;
                natc[2] =  0.7745966692;

                wght[0] =  0.5555555556;
                wght[1] =  0.8888888889;
                wght[2] =  0.5555555556;
            case 4:
                natc[0] = -0.8611363116;
                natc[1] = -0.3399810436;
                natc[2] =  0.3399810436;
                natc[3] =  0.8611363116;

                wght[0] =  0.3478548451;
                wght[1] =  0.6521451549;
                wght[2] =  0.6521451549;
                wght[3] =  0.3478548451;
            case 5:
                natc[0] = -0.9061795459;
                natc[1] = -0.5384693101;
                natc[2] =  0.0000000000;
                natc[3] =  0.5384693101;
                natc[4] =  0.9061795459;

                wght[0] =  0.2369268850;
                wght[1] =  0.4786286705;
                wght[2] =  0.5688888889;
                wght[3] =  0.4786286705;
                wght[4] =  0.2369268850;
            case 6:
                natc[0] = -0.9324695142;
                natc[1] = -0.6612093865;
                natc[2] = -0.2386191861;
                natc[3] =  0.2386191861;
                natc[4] =  0.6612093865;
                natc[5] =  0.9324695142;

                wght[0] =  0.1743244924;
                wght[1] =  0.3607615730;
                wght[2] =  0.4679139346;
                wght[3] =  0.4679139346;
                wght[4] =  0.3607615730;
                wght[5] =  0.1743244924;
            }
    }   
};


float gaussInt( const float x0, const float x1, const int ngp, float (*func)( float ), const int dbg = 0 ){
    float e_mid = 0.5 * (x0 + x1);
    float e_wdt = 0.5 * (x1 - x0);
    float x;
    float I = 0.;
    gaussPoints gpv(ngp);
    for( int i = 0; i < ngp; i++ ){
        x = e_mid + e_wdt * gpv.natc[i];
        I += gpv.wght[i] * func( x );
    }
    return I * e_wdt;
}


/*
//TODO: Romberg Integration
//eps is the fault tolerance
float rombergInt( const float x0, const float x1, const float eps, float (*func)( float ), const int dbg = 0 ){
    int n = 0;
    //float solLow = 0;
    //float solHgh = 0;
    //float Q    = 1;
    int i, steps = 0;
    int j = 1;

    //basic solution matrix (triangluar matrix)
    typedef std::vector<float> result ;
    //initialize with two result vectors
    std::vector<result> res_mat;
    res_mat.assign(2, std::vector<float> );
    res_mat[0].push_back(2);
    res_mat[1].push_back(1);
    res_mat[0][0] = compTrapInt( x0, x1, steps, func );
    res_mat[1][0] = compTrapInt( x0, x1, steps, func );
    res_mat[0][1] = (std::pow(4, n) * res_mat[1][0] - res_mat[0][0]) / (std::pow(4, n) -1);
    while( ( std::abs((res_mat[i][j] - res_mat[i+1][j-1]) / res_mat[i][j]) * 100. > eps ) && ( n < MaxIter ) ){
            res_mat.push_back(1);
            res_mat[res_mat.size()-1].push_back(1);
            for( int k = 0; k < res_mat.size(); k++ ){
                res_mat[k].push_back(1);
            }
            res_mat[0][0] = compTrapInt( x0, x1, steps, func );
            res_mat[1][0] = compTrapInt( x0, x1, steps, func );
            //res_mat[0][1] = (std::pow(4, n) * solHgh - solLow) / (std::pow(4, n) -1);

    }
    float res = res_mat.front()[res_mat.front().begin()];
    return res;
}
*/



} //end namespace NumM
